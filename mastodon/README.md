# Mastodon


```mermaid
flowchart TD
    A[traefik] --> B(Web)
    A --> C(Streaming)
    B --> D(Postgres)
    C --> D
    B --> E(Redis)
    C --> E
    B --> F(OpenSearch)
    D --> G(SideKiq)
    E --> G
    F --> G
    B --> H[S3]
    I[CloudFront] ---> H
```


## Lets Get Started

First of all we need to prepare some directories on the system, so log onto the server where you've already installed traefik. These are going to be used to store persistent data from the containers themselves that needs to survive restarts and reboots.

```bash
mkdir -p /opt/docker/mastodon/opensearch/data
mkdir -p /opt/docker/mastodon/valkey
mkdir -p /opt/docker/mastodon/postgres
mkdir -p /opt/docker/mastodon/public/system
```

Now we are going to pull down the initial docker compose file and create the .env.production file.

```bash
cd /opt/docker/mastodon
wget https://gitlab.com/ric_harvey/fedi-deploy/-/raw/main/mastodon/compose.yml?ref_type=heads
touch .env.production
```

## Setup SES (Simple Email Service)
You're going to need an email service so mastodon can setup accounts, send password reset emails and notifications via email, for this I decided to use [Amazon SES](https://aws.amazon.com/ses/). For this you'll need to:

- Validate a domain
- Set an IAM user up with SMTP details

You can find out how to do that [here](https://aws.amazon.com/ses/getting-started/) or of course you can use another mail server or email service.

> Make sure you download the user details as you'll need these later on. The SMTP user name will look like an AWS Access key rather than being the email address.

## Setup S3 + Create IAM User
Another thing you'll want to do is add storage for uploaded assets. This helps them persist between upgrades and actually gives you the opportunity to scale the web front end without having issues syncing content. It's pretty simple to create an S3 bucket and you can follow this guide [here](https://docs.aws.amazon.com/AmazonS3/latest/userguide/creating-bucket.html), I made sure to set **ACLs enabled** as one of the options the rest were left as default. We won't actually pull the assets directly fromt he bucket but we'll use Cloudfront to do this later.

In order for Mastodon to access the S3 bucket it's going to need some IAM credentials. Now as this setup is running in docker directly and we aren't using an orchestrator such as Kubernetes/EKS or ECS, we can't take advantage of using an IAM role without giving every other container on this EC2 instance access to our S3 bucket, so for this reason we need to generate a set of API/Secret Keys. First we'll make the a [new policy](https://docs.aws.amazon.com/IAM/latest/UserGuide/access_policies_create.html) giving only access to our S3 bucket. The policy should look like this with tweaks for your bucket name.

```json
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "Stmt1718834094231",
      "Action": "s3:*",
      "Effect": "Allow",
      "Resource": "arn:aws:s3:::${BucketName}/*"
    }
  ]
}
```

Now you need to go ahead and [create a new IAM user](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_users_create.html) and attach this policy to it. Once you've done this you'll need to go to the user and generate them a set of API credentials. Don't forget to download these as a CSV as you'll need them later on!

## Setup Cloudfront
As we are going to store assets (images, video, etc) uploaded to a mastodon post in S3, we need to be able to serve them to the general public. This is where Cloudfront comes in. Cloudfront is a CDN (Content Distribution Network) and offloads the process of service the content directly from your server (and in this case S3), by caching the objects closer to the users. Follow [this guide](https://aws.amazon.com/cloudfront/getting-started/S3/) to set it all up. I recommend that you take your time with **step 6** and let Cloudfront generate the config you need that you can then copy to your S3 Bucket policy.

## Configure .env.production for Mastodon

We are now ready to generate our .env.production file, which contains all settings for our own Mastodon Server.

Instead of editing the default .env file from Mastodons GitHub repo, we will use the following setup command, which is an interactive way of generating the config for us.

```bash
cd /opt/docker/mastodon
docker compose run web bundle exec rake mastodon:setup
```

You need to go through and answer the questions. Most of the defaults are correct, however, look out for **redis** as in this tutorial we are going to use the open-source key value store **valkey**.

Once you've walked through the setup wizard you'll be asked if you want to setup the admin user. Answer **Y** and set that up, next you are going to see LOTS of errors come up on the screen. Don't worry, this is happening because we are running inside docker and the script is expecting local or running services. The information you want is in the output and starts with a line like so:

> ```# Generated with mastodon:setup on 2024-06-15 16:41:21 UTC```

```bash
Your instance is identified by its domain name. Changing it afterward will break things.
Domain name: mastodon.<FQDN>

Single user mode disables registrations and redirects the landing page to your public profile.
Do you want to enable single user mode? No

Are you using Docker to run Mastodon? Yes

PostgreSQL host: db
PostgreSQL port: 5432
Name of PostgreSQL database: postgres
Name of PostgreSQL user: postgres
Password of PostgreSQL user: <GENERATE AND ENTER A PASSWORD>
Database configuration works! 

Redis host: valkey
Redis port: 6379
Redis password: <LEAVE THIS BLANK>
Redis configuration works! 

Do you want to store uploaded files on the cloud? No #we'll add this later

Do you want to send e-mails from localhost? No
SMTP server: <MAILSERVER>
SMTP port: 587
SMTP username: <USER>@<MAILSERVER>
SMTP password: <ENTER_YOUR_SMTP_PASSWORD>
SMTP authentication: plain
SMTP OpenSSL verify mode: none
E-mail address to send e-mails "from": Mastodon <notifications@example.com>
Send a test e-mail with this configuration right now? No

This configuration will be written to .env.production
Save configuration? Yes
Below is your configuration, save it to an .env.production file outside Docker:

# Generated with mastodon:setup on 2024-06-15 16:41:21 UTC

LOCAL_DOMAIN=example.com
SINGLE_USER_MODE=false
SECRET_KEY_BASE=XXXXXXXX
OTP_SECRET=XXXXXXXX
VAPID_PRIVATE_KEY=XXXXXXXX
VAPID_PUBLIC_KEY=XXXXXXXX
DB_HOST=db
DB_PORT=5432
DB_NAME=postgres
DB_USER=postgres
DB_PASS=
REDIS_HOST=valkey
REDIS_PORT=6379
REDIS_PASSWORD=
SMTP_SERVER=<MAILSERVER>
SMTP_PORT=587
SMTP_LOGIN=<USER>@<MAILSERVER>
SMTP_PASSWORD=XXXXXXXX
SMTP_AUTH_METHOD=plain
SMTP_OPENSSL_VERIFY_MODE=none
SMTP_FROM_ADDRESS=Mastodon <mastodon@FQDN>

It is also saved within this container so you can proceed with this wizard.

Now that configuration is saved, the database schema must be loaded.
If the database already exists, this will erase its contents.
Prepare the database now? Yes
Running `RAILS_ENV=production rails db:setup` ...

Database 'postgres' already exists
Done!

All done! You can now power on the Mastodon server 🐘

Do you want to create an admin user straight away? Yes
Username: admin
E-mail: admin@example.com
Error connecting to Redis on 127.0.0.1:6379 (Errno::ECONNREFUSED)
Error connecting to Redis on 127.0.0.1:6379 (Errno::ECONNREFUSED)
Switching object-storage-safely from green to red because Redis::CannotConnectError Error connecting to Redis on 127.0.0.1:6379 (Errno::ECONNREFUSED)
Error connecting to Redis on 127.0.0.1:6379 (Errno::ECONNREFUSED)
You can login with the password: XXXXXXXX
You can change your password once you login.
```

Now lets copy that generated config data to a file called **.env.production**. We are going to edit this further to add in an S3 bucket for asset storage and enable *Full Text Search* via OpenSearch, once again we are using the fully open-source fork version of a now propriety system, ElasticSerach.

## Add S3 support

## Enable Full Text Search

## permissions

### web

cd /opt/containers/mastodon
chown -R 991:991 ./public

### opensearch

cd /opt/containers/mastodon
chown -R 1000:root ./opensearch


## enable admin

```bash
docker compose exec web /bin/bash
```

```bash
tootctl accounts deploy -approve <ADMIN_NAME>
```

## enable opensearch

```bash
docker compose exec web /bin/bash
```

```bash
tootctl search deploy
```

## get rid of yellow cluster warning


```bash
docker compose exec os /bin/bash
```


```bash
curl -X PUT -u admin:<OS_PASSWORD> "http://os:9200/_settings" -H 'Content-Type: application/json' -d'{
    "index" : {
        "number_of_replicas" : 0
    }
}'
```

